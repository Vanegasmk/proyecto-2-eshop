@extends('layout')

@section('title','Contact')

@section('content')
<h1>Contact</h1>
<form method="POST" action="{{route('contact')}}">
    @csrf
    <input type="text" name="name" placeholder="nombre" value="{{old('name')}}"><br>
    {!!$errors->first('name','<small>:message </small> <br>')!!} {{-- Muestra si hay un error, al no llenar el espacio --}}
    
    <input type="email" name="email" placeholder="email"><br>
    {!!$errors->first('email','<small>:message </small> <br>')!!}
    
    <input type="text" name="subject" placeholder="asunto"><br>
    {!!$errors->first('subject','<small>:message </small> <br>')!!}
    
    <textarea name="content" id="" cols="30" rows="10" placeholder="mensaje"></textarea><br>
    {!!$errors->first('content','<small>:message </small> <br>')!!}
    
    <button>Enviar</button>
</form>
@endsection