<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <style>
        .active a {
            color: red;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <nav>
        <ul>
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/portafolio">Portafolio</a></li>
            <li><a href="/contact">Contact</a></li>
        </ul>
    </nav>
    @yield('content')
</body>
</html>