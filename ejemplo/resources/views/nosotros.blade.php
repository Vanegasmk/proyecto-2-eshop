@extends('plantilla')

@section('seccion')
    <h1>Nosotros</h1>

    <h1>Este es el equipo de trabajo</h1>
    
    @foreach ($equipo as $persona)
<a href="{{route('nosotros',$persona)}}">{{$persona}}</a><br>
    @endforeach

    @if (!empty($nombre))
        @switch($nombre)
            @case($nombre == "Levin")
                    <h2>El nombre es {{$nombre}}</h2>
                @break
            @case($nombre == "Rosa")
                    <h2>El nombre es {{$nombre}}</h2>
                @break
            @case($nombre == 'María')
                    <h2>El nombre es {{$nombre}}</h2>
            @break
            @default
                
        @endswitch
    @endif
@endsection