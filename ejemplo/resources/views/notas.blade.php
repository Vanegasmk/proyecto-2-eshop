@extends('plantilla')


@section('seccion')
<h1>Notas</h1>

@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif

<form action="{{ route('notas.crear') }}" method="POST">
    @csrf

    <input type="text" name="nombre" placeholder="Nombre" class="form-control mb-2" value="{{old('nombre')}}">
    {!!$errors->first('nombre','<small>:message </small> <br>')!!}

    <input type="text" name="descripcion" placeholder="Descripción" class="form-control mb-2"
        value="{{old('descripcion')}}">
    {!!$errors->first('descripcion','<small>:message </small> <br>')!!}
    <button class="btn btn-primary btn-block" type="submit">Agregar</button>
</form>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripción</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($notas as $nota)
        <tr>
            <th scope="row">{{$nota->id}}</th>

            <td>
                <a href="{{route('notas.detalle', $nota)}}">
                    {{$nota->nombre}}
                </a>
            </td>

            <td>{{$nota->descripcion}}</td>
            <td>
                <a href="{{route('notas.editar',$nota)}}" class="btn btn-primary btn-sm">Editar</a>

                <form action={{route('notas.eliminar',$nota)}} method="POST" class="d-inline">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger btn-sm">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$notas->links()}}
@endsection