@extends('plantilla')


@section('seccion')
<h1>Editar Nota</h1>

@if (session('mensaje'))
    <div class="alert alert-success">
        {{session('mensaje')}}
    </div>
@endif

<form action="{{ route('notas.actualizar',$nota->id) }}" method="POST">
    @method('PUT')
    @csrf

    <input type="text" name="nombre" placeholder="Nombre" class="form-control mb-2" value="{{$nota->id}}">
    {!!$errors->first('nombre','<small>:message </small> <br>')!!} 
    
    <input type="text" name="descripcion" placeholder="Descripción" class="form-control mb-2" value="{{$nota->descripcion}}">
    {!!$errors->first('descripcion','<small>:message </small> <br>')!!} 
    <button class="btn btn-primary btn-block" type="submit">Editar</button>
</form>
@endsection