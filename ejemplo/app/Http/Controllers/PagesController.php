<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App;

class PagesController extends Controller
{

    public function Notas()
    {
        $notas = App\Nota::paginate(2);
        return view('notas',compact('notas'));
    }

    public function Detalle($id)
    {   
        $nota = App\Nota::findOrFail($id);
        return view('notas.detalle',compact('nota'));
    }
    //
    
    public function InicioFotos()
    {
        return view('fotos');
    }

    public function Nosotros($nombre = '')
    {
        $equipo = ['Levin', 'Rosa', 'María'];
        return view('nosotros', compact('equipo', 'nombre'));
    }
    
    public function Blog()
    {
        return view('blog');
    }

    public function CrearNota(Request $request)
    {
        //return $request;

        $request->validate([
            'nombre' => 'required',
            'descripcion' =>'required'
        ]);

        $notaNueva = new App\Nota;

        $notaNueva->nombre = $request->nombre;
        $notaNueva->descripcion = $request->descripcion;

        $notaNueva->save();

        return back()->with('mensaje','Nota agregada!');


    }

    public function EditarNota($id)
    {
        $nota = App\Nota::findOrFail($id);
        return view('notas.editar',compact('nota'));
    }

    public function ActualizarNota(Request $request,$id)
    {
        $notaActualizar = App\Nota::findOrFail($id);
        $notaActualizar->nombre = $request->nombre;
        $notaActualizar->nombre = $request->descripcion;

        $notaActualizar->save();

        return back()->with('mensaje','Nota Actualizada');

    }

    public function EliminarNota($id)
    {
        $notaEliminar = App\Nota::findOrFail($id);
        $notaEliminar->delete();
        
        return back()->with('mensaje','Nota Eliminada');
    }

}
