<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@Notas')->name('notas');

Route::post('/','PagesController@CrearNota')->name('notas.crear'); // Pueden ser de la misma raíz ya que no interfiere por que son diferentes rutas "get" - "post"

Route::get('/detalle/{id}','PagesController@Detalle')->name('notas.detalle');//Para leer los datos mediante el id - se debe de crear una plantilla 

Route::get('/editar/{id}', 'PagesController@EditarNota')->name('notas.editar');

Route::put('/editar/{id}','PagesController@ActualizarNota')->name('notas.actualizar');

Route::delete('/eliminar/{id}', 'PagesController@EliminarNota')->name('notas.eliminar');


Route::get('fotos', 'PagesController@InicioFotos')->name('fotos');

Route::get('blog','PagesController@Blog')->name('blog');

Route::get('nosotros/{nombre?}','PagesController@Nosotros')->name('nosotros');